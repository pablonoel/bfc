## About BFC

Base Frontend Codebase its a SASS/HTML collection of code snippets to create simple, usable and human friendly frontend interfaces.

## Installation

Considering that your project uses npm to manage packages, do the following:

*  `npm install git+ssh://git@bitbucket.org:pablonoel/bfc.git`

*  edit your application main SASS file to import BFC main SASS file with: `@import '~bfc/assets/sass/main.sass'`

If you project is not using node package manager, then just clone this repository inside your project structure and import its main SASS file.
